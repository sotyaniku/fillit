/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 17:52:09 by mdubina           #+#    #+#             */
/*   Updated: 2016/12/10 18:38:48 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_check_tetriminos(char **array, int j, int k)
{
	int	n;

	j = 0;
	n = 0;
	while (array[j])
	{
		k = 0;
		while (array[j][k])
		{
			if (array[j][k] == '#')
			{
				if ((ft_ver(array, j, k, &n) + ft_hor(array, j, k, &n)) == 0)
					return (0);
			}
			k++;
		}
		j++;
	}
	if (n > 5)
		return (1);
	return (0);
}

int		ft_hor(char **array, int j, int k, int *n)
{
	int	count;

	count = *n;
	if (k == 0)
	{
		if (array[j][k + 1] == '#')
			(*n)++;
	}
	else if (k == 3)
	{
		if (array[j][k - 1] == '#')
			(*n)++;
	}
	else
	{
		if (array[j][k + 1] == '#')
			(*n)++;
		if (array[j][k - 1] == '#')
			(*n)++;
	}
	if ((*n) - count != 0)
		return (1);
	return (0);
}

int		ft_ver(char **array, int j, int k, int *n)
{
	int	count;

	count = *n;
	if (j == 0)
	{
		if (array[j + 1][k] == '#')
			(*n)++;
	}
	else if (j == 3)
	{
		if (array[j - 1][k] == '#')
			(*n)++;
	}
	else
	{
		if (array[j - 1][k] == '#')
			(*n)++;
		if (array[j + 1][k] == '#')
			(*n)++;
	}
	if ((*n) - count != 0)
		return (1);
	return (0);
}

int		ft_check_array(char ***array, int i, int j, int k)
{
	int	count;

	while (array[i])
	{
		j = 0;
		count = 0;
		while (array[i][j])
		{
			k = 0;
			while (array[i][j][k])
			{
				if (array[i][j][k] == '#')
					count++;
				k++;
			}
			j++;
		}
		if (count != 4 || !ft_check_tetriminos(array[i], j, k))
			return (0);
		i++;
	}
	return (1);
}
