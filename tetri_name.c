/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tetri_name.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 16:35:44 by mdubina           #+#    #+#             */
/*   Updated: 2016/12/10 18:56:12 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	rename_tetriminos(char ***array)
{
	int i;
	int j;
	int k;

	i = 0;
	while (array[i])
	{
		j = 0;
		while (array[i][j])
		{
			k = 0;
			while (array[i][j][k])
			{
				if (array[i][j][k] == '#')
					array[i][j][k] = 65 + i;
				k++;
			}
			j++;
		}
		i++;
	}
}

void	ft_delete_column(char **array)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (j < 3)
	{
		i = 0;
		while (array[i])
		{
			array[i][j] = array[i][j + 1];
			i++;
		}
		j++;
	}
	i = 0;
	while (i < 4)
	{
		array[i][3] = '.';
		i++;
	}
}

void	ft_delete_line(char **array)
{
	int i;
	int j;

	i = 0;
	while (array[i + 1])
	{
		j = 0;
		while (array[i + 1][j])
		{
			array[i][j] = array[i + 1][j];
			j++;
		}
		i++;
	}
	j = 0;
	while (j < 4)
	{
		array[3][j] = '.';
		j++;
	}
}

char	**ft_cut_tetriminos(char **array)
{
	int j;
	int count;

	while (array[0])
	{
		j = 0;
		count = 0;
		while (array[0][j])
		{
			if (array[0][j] == '.')
				count++;
			j++;
		}
		if (count == 4)
			ft_delete_line(array);
		else
			break ;
	}
	ft_cut_column(array);
	return (array);
}

void	ft_cut_column(char **array)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (array[i])
	{
		if (array[i][0] == '.')
			count++;
		i++;
		if (count == 4)
		{
			ft_delete_column(array);
			i = 0;
			count = 0;
		}
	}
}
