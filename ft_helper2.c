/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_helper2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/10 18:58:11 by mdubina           #+#    #+#             */
/*   Updated: 2016/12/16 08:27:21 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_delete_tetriminos(char **map, int count, char **tetriminos)
{
	int i;
	int j;
	int letters;

	i = 0;
	letters = 0;
	tetriminos[3][3] = '.';
	while (map[i])
	{
		j = 0;
		while (map[i][j])
		{
			if (map[i][j] == 65 + count)
			{
				map[i][j] = '.';
				letters++;
			}
			if (letters == 4)
				return ;
			j++;
		}
		i++;
	}
}

void	ft_free_map(char **map)
{
	int i;

	i = 0;
	while (map[i])
	{
		free(map[i]);
		i++;
	}
	free(map);
	map = NULL;
}
