/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_fill.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 17:48:13 by mdubina           #+#    #+#             */
/*   Updated: 2016/12/10 18:57:26 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		*ft_read(char *file_name)
{
	int		len;
	char	*str;
	int		f;
	char	c;

	if ((f = open(file_name, O_RDONLY)) == -1)
		return (NULL);
	len = 0;
	while (read(f, &c, 1))
		len++;
	str = ft_strnew(len);
	close(f);
	if ((f = open(file_name, O_RDONLY)) == -1)
		return (NULL);
	len = 0;
	while (read(f, &c, 1))
		str[len++] = c;
	str[len] = '\0';
	return (str);
}

int			ft_check_file(char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (str[i] != '#' && str[i] != '.' && str[i] != '\n')
			return (0);
		if ((i % 21 == 4 || i % 21 == 9 || i % 21 == 14 || i % 21 == 19 \
			|| i % 21 == 20) && str[i] != '\n')
			return (0);
		i++;
	}
	if ((i + 1) % 21 != 0)
		return (0);
	return (1);
}

char		***ft_fill_array(char *str, int len, int i, int j)
{
	int		k;
	char	***array;

	if (!(array = (char ***)malloc(sizeof(char **) * (len + 1))))
		return (NULL);
	while (i < len)
	{
		if (!(array[i] = (char **)malloc(sizeof(char *) * 5)))
			return (NULL);
		j = 0;
		while (j < 4)
		{
			if (!(array[i][j] = (char *)malloc(sizeof(char) * 5)))
				return (NULL);
			k = 0;
			while (k < 4)
				array[i][j][k++] = *str++;
			array[i][j++][k] = '\0';
			str++;
		}
		array[i++][j] = 0x0;
		str++;
	}
	array[i] = 0x0;
	return (array);
}

char		***ft_helper(char *str)
{
	int		i;
	int		j;
	int		k;
	int		len;
	char	***array;

	i = 0;
	j = 0;
	k = 0;
	len = (ft_strlen(str) + 1) / 21;
	array = ft_fill_array(str, len, i, j);
	if (ft_check_array(array, i, j, k))
	{
		while (i < len)
		{
			ft_cut_tetriminos(array[i]);
			i++;
		}
		return (array);
	}
	return (NULL);
}
