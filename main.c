/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/06 13:39:23 by mdubina           #+#    #+#             */
/*   Updated: 2016/12/10 18:46:24 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			main(int argc, char **argv)
{
	char	*str;
	char	***array;

	if (argc != 2)
		ft_putstr("usage: ./fillit file_name\n");
	else
	{
		if ((str = ft_read(argv[1])) == NULL || ft_check_file(str) == 0)
		{
			ft_putstr("error\n");
			return (1);
		}
		if ((array = ft_helper(str)) == NULL)
		{
			ft_putstr("error\n");
			return (1);
		}
		rename_tetriminos(array);
		ft_fillit(array);
	}
	return (0);
}
