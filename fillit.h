/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/07 14:23:55 by mdubina           #+#    #+#             */
/*   Updated: 2016/12/16 09:01:05 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>

void	ft_putchar(char c);
void	ft_putstr(const char *s);
size_t	ft_strlen(const char *s);
char	*ft_strnew(size_t size);
int		ft_sqrt(char ***array);
void	ft_show_map(char **map);
char	**ft_create_map(int sqrt);
int		ft_check_tetriminos(char **array, int j, int k);
int		ft_hor(char **array, int j, int k, int *n);
int		ft_ver(char **array, int j, int k, int *n);
int		ft_check_array(char ***array, int i, int j, int k);
char	*ft_read(char *filename);
int		ft_check_file(char *str);
char	***ft_fill_array(char *str, int len, int i, int j);
char	***ft_helper(char *str);
char	**ft_cut_tetriminos(char **array);
void	ft_delete_line(char **array);
void	ft_delete_column(char **array);
void	ft_cut_column(char **array);
void	ft_push_tetriminos(char **array, char **map, int i, int j);
int		ft_check_map_tetriminos(char **map, char **tetriminos, int i, int j);
int		ft_fillit(char ***array);
int		ft_search(char **map, int *i, int *j);
int		ft_fill_map(char **map, char ***array, int count, int i);
void	rename_tetriminos(char ***array);
void	ft_delete_tetriminos(char **map, int count, char **tetriminos);
void	ft_free_map(char **map);

#endif
