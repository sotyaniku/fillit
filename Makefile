#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mdubina <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/12/10 13:22:45 by mdubina           #+#    #+#              #
#    Updated: 2016/12/16 09:01:19 by ksarnyts         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = fillit

HEADER = fillit.h

CFLAG = -Wall -Wextra -Werror

SRC = check.c map_cut.c tetri_name.c read_fill.c fillit.c ft_helper2.c \
		ft_helper3.c

OBJ = $(SRC:.c=.o)

all: $(NAME)

$(NAME): 
	gcc $(CFLAG) -c $(SRC)
	ar rc $(NAME).a $(OBJ)
	ranlib $(NAME).a
	gcc -o $(NAME) main.c $(NAME).a

clean:
	rm -f $(OBJ) $(NAME).a

fclean: clean
	rm -f $(NAME)

re: fclean all
