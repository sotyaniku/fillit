/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 17:48:16 by mdubina           #+#    #+#             */
/*   Updated: 2016/12/16 12:45:39 by ksarnyts         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdio.h>

void		ft_push_tetriminos(char **tetriminos, char **map, int i, int j)
{
	int		b;
	int		z;
	int		count;

	count = 0;
	while (*tetriminos && map[i])
	{
		b = 0;
		z = j;
		while ((*tetriminos)[b] && map[i][z])
		{
			if ((*tetriminos)[b] >= 65)
			{
				map[i][z] = (*tetriminos)[b];
				count++;
			}
			if (count == 4)
				return ;
			b++;
			z++;
		}
		tetriminos++;
		i++;
	}
}

int			ft_fillit(char ***array)
{
	int		n;
	char	**map;
	int		i;
	int 	count;

	n = 0;
	i = 0;
	count = 0;
	while (n < 4)
	{
		map = ft_create_map(ft_sqrt(array) + n);
		if (ft_fill_map(map, array, count, i) == 1)
		{
			ft_show_map(map);
			return (1);
		}
		i = 0;
		ft_free_map(map);
		n++;
	}
	return (0);
}

int			ft_fill_map(char **map, char ***array, int count, int i)
{
	int j;
	
	if (!array[count])
		return (1);
	i = 0;
	while (map[i])
	{
		j = 0;
		while (map[i][j])
		{
			if (ft_check_map_tetriminos(map, array[count], i, j))
			{
				ft_push_tetriminos(array[count], map, i, j);
				if (ft_fill_map(map, array, count + 1, i) == 1)
					return (1);
				ft_delete_tetriminos(map, count, array[count]);
			}
			j++;
		}
		i++;
	}
	return (0);
}

int			ft_check_map_tetriminos(char **map, char **tetriminos, int i, int j)
{
	int a;
	int b;
	int z;
	int count;

	a = 0;
	count = 0;
	z = j;
	while (tetriminos[a] && map[i])
	{
		b = 0;
		while (tetriminos[a][b] && map[i][z] && (a + b) <= 3)
		{
			if (tetriminos[a][b] >= 65 && map[i][z] == '.')
				count++;
			b++;
			z++;
		}
		if (count == 4)
			return (1);
		z = j;
		a++;
		i++;
	}
	return (0);
}
