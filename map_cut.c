/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_cut.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdubina <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/08 12:21:31 by mdubina           #+#    #+#             */
/*   Updated: 2016/12/10 18:55:09 by mdubina          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		ft_sqrt(char ***array)
{
	int i;
	int n;
	int b;

	i = 0;
	while (array[i])
		i++;
	n = i * 4;
	i = 1;
	b = 0;
	while (i <= n / 2)
	{
		b = i * i;
		if (b >= n)
			return (i);
		i = i + 1;
	}
	return (0);
}

void	ft_show_map(char **map)
{
	int i;
	int j;

	i = 0;
	while (map[i])
	{
		j = 0;
		while (map[i][j])
		{
			ft_putchar(map[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

char	**ft_create_map(int sqrt)
{
	int		i;
	int		j;
	char	**map;

	if (!(map = (char **)malloc(sizeof(char *) * (sqrt + 1))))
		return (NULL);
	i = 0;
	while (i < sqrt)
	{
		if (!(map[i] = (char *)malloc(sizeof(char) * (sqrt + 1))))
			return (NULL);
		j = 0;
		while (j < sqrt)
		{
			map[i][j] = '.';
			j++;
		}
		map[i][j] = '\0';
		i++;
	}
	map[i] = 0x0;
	return (map);
}
